from django.urls import re_path

from APP.app02 import views

urlpatterns = [
    re_path('^index/(?P<num>[0-9]){3}/$', views.index),
]
