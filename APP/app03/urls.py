from django.urls import path

from APP.app03 import views

urlpatterns = [
    path('index/', views.index, name='index')
]
