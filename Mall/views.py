import datetime

from django.http import HttpResponse

# Default
from django.shortcuts import render
from django.urls import reverse


# 不带参数的返回
def index(request):
    # url = reverse('home')
    url = reverse("app03:index")  # reverse()：反向解析URL地址
    return HttpResponse("The URL is: " + url)


# 带参数的返回
def index_para(request, year):
    return HttpResponse("The URL is: " + year)


# 使用render()方法在网页模板中返回字符串
def demo(request):
    context = {'hello': 'Hello world!'}
    return render(request, 'templates/demo.html', context)


# 通过列表的方式进向模板提交数据
def demo_list(request):
    view_list = ['norm', 'wesley', 'dawngrasp']
    return render(request, 'demo_list.html', {"view_list": view_list})


# 通过字典的方式进向模板提交数据
def demo_dict(request):
    view_dict = {
        "name": "norm",
        "sex": "male",
        "age": 23
    }
    return render(request, 'demo_dict.html', {"view_dict": view_dict})


# 使用过滤器过滤字符串
def flitter(request):
    dict = {
        "name": "norm",
        "sex": "male",
        "age": 23
    }
    return render(request, 'flitter.html', {"flitter": "hello, world! My name is Norm.",
                                            "bool": False,
                                            "num": 88,
                                            "time": datetime.datetime.now(),
                                            "link": "<a href=\'https://www.google.com\'>Click it</a>",
                                            "view_list": ['norm', 'wesley', 'dawngrasp'],
                                            "dict": dict,
                                            "listvar": [],  # ["a", "b", "d", "c", "e"]
                                            "cmp1": True,
                                            "cmp2": False,
                                            })


# GET
def test_GET(request):
    var = request.GET.get("name")
    return HttpResponse('姓名：{}'.format(name))
