#!opt/homebrew/bin/python3.9
# -*- coding: utf-8 -*-

"""
@Project: Mall
@File: test_db.py
@Author: norm_x
@Time: 2021/10/03
@Version: 1.0
@Function: None
@Modify: None
"""
from django.http import HttpResponse

from TestModel.models import Test


# 测试连接状态
def test_connection(request):
    test_db = Test(name='test')
    test_db.save()
    return HttpResponse("success")


# 查询
def select_all(request):
    # 初始化
    res = ""
    res_items = ""

    # 语句：Select * from [TableName]
    list = Test.objects.all()

    # 条件：相当于Where过滤器
    # res_where = Test.objects.flitter(id=1)

    # 获取单个对象
    # res_single = Test.objects.get(id=1)

    # 限制返回数据，等价于SQL中的OFFSET 0 LIMIT 2
    # res_order = Test.objects.order_by('name')[0:2]

    # 排序
    res_sort = Test.objects.order_by("name")

    # 连锁使用
    # res_status = Test.objects.flitter(name="test").order_by("id")

    # 更新
    res_update = Test.objects.get(name="root")
    res_update.name = 'super_user'
    res_update.save()

    # 删除
    res_delete = Test.objects.get(name="test")
    res_delete.delete()

    # 输出
    for items in list:
        res_items += items.name + '<br>'
    res = res_items
    return HttpResponse(res_sort)
