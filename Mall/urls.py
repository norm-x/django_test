"""Mall URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include

from Mall import views, testdb

urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/test', views.index, name='home'),
    # ============================================
    path('demo/', views.demo),
    path('demo_list/', views.demo_list),
    path('demo_dict/', views.demo_dict),
    path('flitter/', views.flitter),
    # ============================================
    path('testdb/connect', testdb.test_connection),
    path('testdb/select', testdb.select_all),
    # ============================================
    # re_path(r'^index_para/(?P<year>[0-9]{4})/$', views.index_para),
    # path('app01/', include("APP.app01.urls")),
    # path('app01/', include("APP.app01.urls", namespace=None)),
    # path('app02/', include("APP.app02.urls")),
    # # 2.0之后的版本namespace参数是内嵌在include方法内部
    # path('app03/', include(("app03.urls", 'APP.app03'))),
]
